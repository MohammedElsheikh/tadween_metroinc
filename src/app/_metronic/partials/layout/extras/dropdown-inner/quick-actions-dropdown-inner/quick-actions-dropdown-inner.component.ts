import { AddRepresentativeComponent } from './../../../../../../representatives/add-representative/add-representative.component';
import { AddProductComponent } from './../../../../../../products/add-product/add-product.component';
import { FormBuilder } from '@angular/forms';
import { AddVoucherComponent } from './../../../../../../vouchers/add-voucher/add-voucher.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../../../../../core';

@Component({
  selector: 'app-quick-actions-dropdown-inner',
  templateUrl: './quick-actions-dropdown-inner.component.html',
  styleUrls: ['./quick-actions-dropdown-inner.component.scss'],
})
export class QuickActionsDropdownInnerComponent implements OnInit {
  extrasQuickActionsDropdownStyle: 'light' | 'dark' = 'light';
  constructor(
    private fb: FormBuilder,
    private layout: LayoutService,
    private modalService: NgbModal) {}

  ngOnInit(): void {
    this.extrasQuickActionsDropdownStyle = this.layout.getProp(
      'extras.quickActions.dropdown.style'
    );
  }

  createVoucher() {
    this.AddVoucher(undefined);
  }
  AddVoucher(id: number) {
    const modalRef = this.modalService.open(AddVoucherComponent, { size: 'xl' });
    modalRef.componentInstance.id = id;
  }

  createProduct() {
    this.AddProduct(undefined);
  }
  AddProduct(id: number) {
    const modalRef = this.modalService.open(AddProductComponent, { size: 'lg' });
    modalRef.componentInstance.id = id;
  }

  createRepresentative() {
    this.AddRepresentative(undefined);
  }
  AddRepresentative(id: number) {
    const modalRef = this.modalService.open(AddRepresentativeComponent, { size: 'lg' });
    modalRef.componentInstance.id = id;
  }
}
