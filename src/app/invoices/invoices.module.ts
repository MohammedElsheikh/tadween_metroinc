import { InvoicesRoutingModule } from './invoices-routing.module';
import { AddInvoiceComponent } from './add-invoice/add-invoice.component';
import { ListInvoicesComponent } from './list-invoices/list-invoices.component';
import { NgbModalModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { CRUDTableModule } from './../_metronic/shared/crud-table/crud-table.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InlineSVGModule } from 'ng-inline-svg';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownModule } from 'primeng/dropdown';
@NgModule({
  declarations: [AddInvoiceComponent, ListInvoicesComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    InvoicesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    InlineSVGModule,
    CRUDTableModule,
    NgbModalModule,
    NgbDatepickerModule,
    DropdownModule
  ]
})
export class InvoicesModule { }
