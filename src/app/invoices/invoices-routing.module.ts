import { AddInvoiceComponent } from './add-invoice/add-invoice.component';
import { ListInvoicesComponent } from './list-invoices/list-invoices.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ListInvoicesComponent,
  },
  {
    path: 'list-invoices',
    component: ListInvoicesComponent,
  },
  {
    path: 'add-invoice',
    component: AddInvoiceComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicesRoutingModule { }
