import { Component, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-add-invoice",
  templateUrl: "./add-invoice.component.html",
  styleUrls: ["./add-invoice.component.scss"],
})
export class AddInvoiceComponent implements OnInit {
  selectedCountry = {} as any;
  countries: any[];
  step =1;
  constructor(public modal: NgbActiveModal) {
    this.countries = [{ name: "first choice" }, { name: "second choice" }];
  }

  ngOnInit(): void {}
  stepTwo(){
    this.step = 2;
  }
  stepOne(){
    this.step = 1;
  }
}
