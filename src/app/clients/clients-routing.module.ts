import { AddClientComponent } from './add-client/add-client.component';
import { ListClientsComponent } from './list-clients/list-clients.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'add-client',
        component: AddClientComponent,
  },
  {
    path: 'list-clients',
    component: ListClientsComponent,
  },
  {
    path: '',
    component: ListClientsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
