import { ClientsRoutingModule } from './clients-routing.module';
import { ListClientsComponent } from './list-clients/list-clients.component';
import { VouchersRoutingModule } from './../vouchers/vouchers-routing.module';
import { AddClientComponent } from './add-client/add-client.component';



import { NgbModalModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { CRUDTableModule } from './../_metronic/shared/crud-table/crud-table.module';
import { ECommerceRoutingModule } from './../modules/e-commerce/e-commerce-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InlineSVGModule } from 'ng-inline-svg';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [ListClientsComponent, AddClientComponent],
  imports: [
    CommonModule,
    CommonModule,
    HttpClientModule,
ClientsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    InlineSVGModule,
    CRUDTableModule,
    NgbModalModule,
    NgbDatepickerModule,
    DropdownModule
  ]
})
export class ClientsModule {}