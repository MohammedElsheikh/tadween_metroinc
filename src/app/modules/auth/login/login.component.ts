import { HelperService } from "./../../../services/helper.service";
import { AuthService } from "./../../../services/auth.service";
import { Component, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Subscription, Observable } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  // KeenThemes mock, change it to:
  // defaultAuth = {
  //   email: '',
  //   password: '',
  // };
  countries: any[];
  selectedCountry = {} as any;
  phone;
  password;
  inputPassword = true;
  user = {} as any;

  counries = ["002", "+997"];
  defaultAuth: any = {
    email: "admin@demo.com",
    password: "demo",
  };
  hasError: boolean;
  returnUrl: string;
  isLoading: boolean;

  constructor(
    private authService: AuthService,
    private router: Router,
    private helper: HelperService
  ) {
    if (authService.isLoggedIn()) {
      this.router.navigate(["/"]);
    }
    this.countries = [
      { flag: "../../../assets/img/egypt.svg", code: "+20", name: "+20" },
      { flag: "../../../assets/img/ksa.svg", code: "+996", name: "+996" },
    ];
  }

  ngOnInit(): void {}

  async loginForm() {
    try {
      console.log(this.user);
      const formData = new FormData();
      // tslint:disable-next-line: forin
      for (const key in this.user) {
        formData.append(key, this.user[key]);
      }
      this.hasError = false;
      this.isLoading = true;
      const res: any = await this.authService.login(formData);
      console.log(res);

      if (res.status) {
        this.router.navigate(["/dashboard"]);
        localStorage.setItem("token", "Bearer " + res.data.api_token);
        localStorage.setItem("user", JSON.stringify(res.data.user));
        localStorage.setItem("type", JSON.stringify(res.data.user.type));
        this.isLoading = false;

        this.helper.successAlert(res.message);
      } else {
        this.helper.errorAlert(res.message);
        this.isLoading = false;
        this.hasError = true;
      }
    } catch (err) {
      this.isLoading = false;
      console.log(err);
      this.hasError = true;
    }
  }
}
