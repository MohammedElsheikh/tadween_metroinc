import { AuthService } from "./../../../services/auth.service";
import { HelperService } from "./../../../services/helper.service";
import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { MouseEvent } from "@agm/core";

@Component({
  selector: "app-registration",
  templateUrl: "./registration.component.html",
  styleUrls: ["./registration.component.scss"],
})
export class RegistrationComponent implements OnInit {
  step = 1;
  // mobNumberPattern = '^((\\+91-?)|0)?[0-9]{10}$';
  countries: any[];
  selectedCountry = [] as any;
  mobNumberPattern;
  users = [];
  inputPassword = true;
  inputPassword2 = true;
  inputPassword3 = true;
  inputPassword4 = true;
  new_password;
  registerData = {
    owner_type: "company",
  } as any;
  mapCenter = {
    lat: 30.033333,
    lng: 31.233334,
  };

  lat;
  lng;
  zoom = 15;
  representativeId;
  markers: marker[] = [];
  vaild;
  constructor(
    private helper: HelperService,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.userType();
    this.countries = [
      { flag: "../../../assets/img/egypt.svg", code: "+20", name: "+20" },
      { flag: "../../../assets/img/ksa.svg", code: "+996", name: "+996" },
    ];
  }
  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`);
  }

  mapClicked($event: MouseEvent) {
    this.markers = [
      {
        lat: $event.coords.lat,
        lng: $event.coords.lng,
        draggable: true,
      },
    ];
    this.lat = this.markers[0].lat;
    this.lng = this.markers[0].lng;
    console.log(this.lat, this.lng);
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log("dragEnd", m, $event);
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;
  }

  CheckPassword() {
    if (this.registerData.password !== this.new_password) {
      this.helper.errorAlert("The Password not matching");
    } else {
      this.registerStep1();
    }
  }

  async create() {
    this.registerData.longitude = this.lng + "";
    this.registerData.latitude = this.lat + "";
    this.registerData.branch_id += "";
    this.registerData.commercial_registered += "";
    this.registerData.phone += "";
    this.registerData.type += "";
    this.registerData.vat_number += "";
    try {
      console.log(this.registerData);
      const res: any = await this.auth.createAccount(this.registerData);
      console.log(res);
      this.helper.successAlert(res.message);
      this.router.navigate(["/verify-account/" + this.registerData.phone]);
    } catch (err) {
      console.log(err);
    }
  }
  async findRepresentativePhone(phone) {
    try {
      const res: any = await this.auth.findRepresentative(phone);

      if (res.data != null) {
        this.step = 4;
        this.representativeId = res.data.id;
      } else {
        this.helper.errorAlert("You Are Not Registered");
      }
    } catch (err) {
      console.log(err);
    }
  }
  async send() {
    try {
      if (this.registerData.password !== this.new_password) {
        this.helper.errorAlert("The Password not matching");
      } else {
        const body = {
          name: this.registerData.name,
          password: this.registerData.password,
          profile_code: this.registerData.profile_code,
          type: this.registerData.type,
        };
        const res: any = await this.auth.complete(this.representativeId, body);
        console.log(res);
        if (status) {
          this.helper.errorAlert(res.message);
        } else {
          this.helper.successAlert("Success!");
          this.router.navigate(["/verify-account/" + this.registerData.phone]);
        }
        console.log(this.registerData);
      }
    } catch (err) {
      console.log(err);
    }
  }
  async userType() {
    try {
      const res: any = await this.auth.type();
      res.data.map((e) => {
        console.log(encodeURIComponent);
        if (e.id != 3 && e.id != 4) {
          this.users.push({
            label: e.name,
            value: e.id,
          });
        }
      });
      this.registerData.type = 1;
      console.log(this.users);
    } catch (err) {
      console.log(err);
    }
  }
  async registerStep1() {
    try {
      const res: any = await this.auth.step3({
        phone: this.registerData.phone + "",
      });
      if (res.status) {
        this.step = 2;
      } else {
        this.helper.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
  async registerStep2() {
    try {
      const res: any = await this.auth.step2({
        type: this.registerData.type + "",
        vat_number: this.registerData.vat_number + "",
        commercial_registered: this.registerData.commercial_registered + "",
        branch_id: this.registerData.branch_id + "",
        contact_info: this.registerData.contact_info + "",
      });
      if (res.status) {
        this.step = 3;
      } else {
        this.helper.errorAlert(res.message);
      }
    } catch (err) {
      console.log(err);
    }
  }
}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
