import { NgbModalModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { CRUDTableModule } from './../_metronic/shared/crud-table/crud-table.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InlineSVGModule } from 'ng-inline-svg';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { VouchersRoutingModule } from './vouchers-routing.module';
import { ListVouchersComponent } from './list-vouchers/list-vouchers.component';
import { AddVoucherComponent } from './add-voucher/add-voucher.component';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [ListVouchersComponent, AddVoucherComponent],
  imports: [
    CommonModule,
    VouchersRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    InlineSVGModule,
    CRUDTableModule,
    NgbModalModule,
    NgbDatepickerModule,
    DropdownModule
  ]
})
export class VouchersModule { }
