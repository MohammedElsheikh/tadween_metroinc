import { CustomersService } from "./../../modules/e-commerce/_services/fake/customers.service";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { from, Subscription } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import {
  GroupingState,
  PaginatorState,
  SortState,
  ICreateAction,
  IEditAction,
  IDeleteAction,
  IDeleteSelectedAction,
  IFetchSelectedAction,
  IUpdateStatusForSelectedAction,
  ISortView,
  IFilterView,
  IGroupingView,
  ISearchView,
} from "../../_metronic/shared/crud-table";
import { DeleteCustomerModalComponent } from "./../../modules/e-commerce/customers/components/delete-customer-modal/delete-customer-modal.component";
import { DeleteCustomersModalComponent } from "./../../modules/e-commerce/customers/components/delete-customers-modal/delete-customers-modal.component";
import { UpdateCustomersStatusModalComponent } from "./../../modules/e-commerce/customers/components/update-customers-status-modal/update-customers-status-modal.component";
import { FetchCustomersModalComponent } from "./../../modules/e-commerce/customers/components/fetch-customers-modal/fetch-customers-modal.component";
import { AddVoucherComponent } from "./../add-voucher/add-voucher.component";
import {
  NgbModal,
  NgbActiveModal,
  ModalDismissReasons,
  NgbModalOptions,
} from "@ng-bootstrap/ng-bootstrap";

const modalWithDefaultOptions = {
  beforeCodeTitle: "Modal with default options",
  htmlCode: `
<div class="example-preview">
  <span *ngIf="closeResult">
    <pre>{{closeResult}}</pre>
  </span>
  <div>
    <ng-template #content let-c="close" let-d="dismiss">
      <div class="modal-header">
        <h4 class="modal-title">Basic demo</h4>
        <button type="button" class="close" aria-label="Close" (click)="d('Cross click')">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
		  standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a
		  type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining
		  essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
          passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" (click)="c('Close click')">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </ng-template>
    <button class="btn btn-primary" (click)="open(content)">Launch demo modal</button>
  </div>
</div>
`,
  tsCode: `
import {Component} from '@angular/core';\n
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';\n
@Component({
    selector: 'ngbd-modal-basic',
    templateUrl: './modal-basic.html'
})
export class NgbdModalBasic {
    closeResult: string;\n
    constructor(private modalService: NgbModal) {}\n
    open(content) {
        this.modalService.open(content).result.then((result) => {
        this.closeResult = \`Closed with: $\{result\}\`;
        }, (reason) => {
            this.closeResult = \`Dismissed $\{this.getDismissReason(reason)\}\`;
        });
    }\n
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  \`with: $\{reason}\`;
        }
    }
}
`,
  isCodeVisible: false,
  isExampleExpanded: true,
};

@Component({
  selector: "app-list-vouchers",
  templateUrl: "./list-vouchers.component.html",
  styleUrls: ["./list-vouchers.component.scss"],
})
export class ListVouchersComponent
  implements
    OnInit,
    OnDestroy,
    ICreateAction,
    IEditAction,
    IDeleteAction,
    IDeleteSelectedAction,
    IFetchSelectedAction,
    IUpdateStatusForSelectedAction,
    ISortView,
    IFilterView,
    IGroupingView,
    ISearchView,
    IFilterView {
  closeResult: string;
  exampleModalWithDefaultOptions;
  paginator: PaginatorState;
  sorting: SortState;
  grouping: GroupingState;
  isLoading: boolean;
  filterGroup: FormGroup;
  searchGroup: FormGroup;
  private subscriptions: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    public customerService: CustomersService
  ) {}

  // angular lifecircle hooks
  ngOnInit(): void {
    this.exampleModalWithDefaultOptions = modalWithDefaultOptions;
    this.filterForm();
    this.searchForm();
    this.customerService.fetch();
    this.grouping = this.customerService.grouping;
    this.paginator = this.customerService.paginator;
    this.sorting = this.customerService.sorting;
    const sb = this.customerService.isLoading$.subscribe(
      (res) => (this.isLoading = res)
    );
    this.subscriptions.push(sb);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sb) => sb.unsubscribe());
  }

  // filtration
  filterForm() {
    this.filterGroup = this.fb.group({
      status: [""],
      type: [""],
      searchTerm: [""],
    });
    this.subscriptions.push(
      this.filterGroup.controls.status.valueChanges.subscribe(() =>
        this.filter()
      )
    );
    this.subscriptions.push(
      this.filterGroup.controls.type.valueChanges.subscribe(() => this.filter())
    );
  }

  filter() {
    const filter = {};
    const status = this.filterGroup.get("status").value;
    if (status) {
      filter["status"] = status;
    }

    const type = this.filterGroup.get("type").value;
    if (type) {
      filter["type"] = type;
    }
    this.customerService.patchState({ filter });
  }

  // search
  searchForm() {
    this.searchGroup = this.fb.group({
      searchTerm: [""],
    });
    const searchEvent = this.searchGroup.controls.searchTerm.valueChanges
      .pipe(
        /*
      The user can type quite quickly in the input box, and that could trigger a lot of server requests. With this operator,
      we are limiting the amount of server requests emitted to a maximum of one every 150ms
      */
        debounceTime(150),
        distinctUntilChanged()
      )
      .subscribe((val) => this.search(val));
    this.subscriptions.push(searchEvent);
  }

  search(searchTerm: string) {
    this.customerService.patchState({ searchTerm });
  }

  // sorting
  sort(column: string) {
    const sorting = this.sorting;
    const isActiveColumn = sorting.column === column;
    if (!isActiveColumn) {
      sorting.column = column;
      sorting.direction = "asc";
    } else {
      sorting.direction = sorting.direction === "asc" ? "desc" : "asc";
    }
    this.customerService.patchState({ sorting });
  }

  // pagination
  paginate(paginator: PaginatorState) {
    this.customerService.patchState({ paginator });
  }

  // form actions
  create() {
    this.AddVoucher(undefined);
  }
  AddVoucher(id: number) {
    const modalRef = this.modalService.open(AddVoucherComponent, {
      size: "xl",
    });
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => this.customerService.fetch(),
      () => {}
    );
  }

  edit(id: number) {
    const modalRef = this.modalService.open(AddVoucherComponent, {
      size: "xl",
    });
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => this.customerService.fetch(),
      () => {}
    );
  }

  delete(id: number) {
    const modalRef = this.modalService.open(DeleteCustomerModalComponent);
    modalRef.componentInstance.id = id;
    modalRef.result.then(
      () => this.customerService.fetch(),
      () => {}
    );
  }

  deleteSelected() {
    const modalRef = this.modalService.open(DeleteCustomersModalComponent);
    modalRef.componentInstance.ids = this.grouping.getSelectedRows();
    modalRef.result.then(
      () => this.customerService.fetch(),
      () => {}
    );
  }

  updateStatusForSelected() {
    const modalRef = this.modalService.open(
      UpdateCustomersStatusModalComponent
    );
    modalRef.componentInstance.ids = this.grouping.getSelectedRows();
    modalRef.result.then(
      () => this.customerService.fetch(),
      () => {}
    );
  }

  fetchSelected() {
    const modalRef = this.modalService.open(FetchCustomersModalComponent);
    modalRef.componentInstance.ids = this.grouping.getSelectedRows();
    modalRef.result.then(
      () => this.customerService.fetch(),
      () => {}
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
  open(content) {
    this.modalService.open(content, { size: "lg" }).result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
}
