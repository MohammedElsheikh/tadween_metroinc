import { AddVoucherComponent } from './add-voucher/add-voucher.component';
import { ListVouchersComponent } from './list-vouchers/list-vouchers.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ListVouchersComponent
  },
  {
    path: 'list-vouchers',
    component: ListVouchersComponent,
  },
  {
    path: 'add-voucher',
    component: AddVoucherComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VouchersRoutingModule { }
