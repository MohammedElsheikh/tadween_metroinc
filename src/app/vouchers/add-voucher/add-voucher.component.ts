import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-add-voucher',
  templateUrl: './add-voucher.component.html',
  styleUrls: ['./add-voucher.component.scss'],
  // NOTE: For this example we are only providing current component, but probably
  // NOTE: you will w  ant to provide your main App Module
})
export class AddVoucherComponent implements OnInit {
  selectedCountry = {}  as any;
  countries: any[];
  constructor(
    private fb: FormBuilder, public modal: NgbActiveModal
    ) { 
      this.countries = [
        {name: 'first choice'},
        {name: 'second choice'}
      ];
    }

  ngOnInit(): void {
  }
}
