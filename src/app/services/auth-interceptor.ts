import { Injectable, Injector } from "@angular/core";
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

import { Router } from "@angular/router";
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private router: Router) {}
  /**
   * @param  {HttpRequest<any>} req
   * @param  {HttpHandler} next
   * @returns Observable
   * injects token into any http request made into the backend
   */
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = window.localStorage.getItem("token") || "";
    const authReq = req.clone({
      setHeaders: {
        Authorization: token,
      },
    });
    return next.handle(authReq);
  }
}
