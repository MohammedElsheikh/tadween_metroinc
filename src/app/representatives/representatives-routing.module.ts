import { AddRepresentativeComponent } from './add-representative/add-representative.component';
import { ListRepresentativesComponent } from './list-representatives/list-representatives.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ListRepresentativesComponent
  },
  {
    path: 'list-representatives',
    component: ListRepresentativesComponent,
  },
  {
    path: 'add-representative',
    component: AddRepresentativeComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepresentativesRoutingModule { }
