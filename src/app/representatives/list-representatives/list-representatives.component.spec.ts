import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRepresentativesComponent } from './list-representatives.component';

describe('ListRepresentativesComponent', () => {
  let component: ListRepresentativesComponent;
  let fixture: ComponentFixture<ListRepresentativesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListRepresentativesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRepresentativesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
