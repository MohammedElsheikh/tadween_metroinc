import { RepresentativesRoutingModule } from './representatives-routing.module';
import { AddRepresentativeComponent } from './add-representative/add-representative.component';
import { ListRepresentativesComponent } from './list-representatives/list-representatives.component';


import { NgbModalModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { CRUDTableModule } from './../_metronic/shared/crud-table/crud-table.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InlineSVGModule } from 'ng-inline-svg';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownModule } from 'primeng/dropdown';
@NgModule({
  declarations: [AddRepresentativeComponent, ListRepresentativesComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    RepresentativesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    InlineSVGModule,
    CRUDTableModule,
    NgbModalModule,
    NgbDatepickerModule,
    DropdownModule
  ]
})
export class RepresentativesModule { }
