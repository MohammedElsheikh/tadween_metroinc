import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-representative',
  templateUrl: './add-representative.component.html',
  styleUrls: ['./add-representative.component.scss'],
  // NOTE: For this example we are only providing current component, but probably
  // NOTE: you will w  ant to provide your main App Module
})
export class AddRepresentativeComponent implements OnInit {
  selectedCountry = {}  as any;
  countries: any[];
  constructor(
    private fb: FormBuilder, public modal: NgbActiveModal
    ){
      this.countries = [
        {name: '+966', code: 'AU',img:"../../../../assets/img/ksa.svg"},
        {name: '02', code: 'BR',img:"../../../../assets/img/egypt.svg"}
    ];
    }

  ngOnInit(): void {
  }
}
